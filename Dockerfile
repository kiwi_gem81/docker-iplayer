# This will be my iplayer container

FROM centos:6

MAINTAINER Tim Moor <tim.moor@linux-geek.co.nz>

RUN yum -y install epel-release && rpm -Uvh http://li.nux.ro/download/nux/dextop/el6/x86_64/nux-dextop-release-0-2.el6.nux.noarch.rpm
RUN yum -y install get_iplayer unzip perl-XML-Simple perl-Authen-SASL perl-Net-SMTP-SSL git perl-CGI
RUN yum -y update

RUN yum -y install automake autoconf gcc-c++ zlib-devel && \
    curl -kLO https://bitbucket.org/wez/atomicparsley/get/tip.zip && \
    unzip tip.zip && \
    cd wez* && \
    bash autogen.sh && \
    bash configure && \
    make && \
    make install
    
RUN yum clean all
    
# Define mountable directories.
VOLUME ["/data"]

# Define working directory.
WORKDIR /data

# Add start file to container
ADD "start.sh" "/data/start.sh"

# Define default command.
CMD bash /data/start.sh

# Expose ports.
EXPOSE 1935