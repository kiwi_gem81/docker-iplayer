#!/bin/bash

cd /data/

if [ -e /data/get_iplayer/get_iplayer.cgi ]; then
	chmod u+x /data/get_iplayer/get_iplayer.cgi
	perl /data/get_iplayer/get_iplayer.cgi --port 1935 --getiplayer /usr/bin/get_iplayer 
else
    git clone https://github.com/get-iplayer/get_iplayer.git
    chmod u+x /data/get_iplayer/get_iplayer.cgi
    perl /data/get_iplayer/get_iplayer.cgi --port 1935 --getiplayer /usr/bin/get_iplayer 
fi
