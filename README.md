## docker - get_iplayer: BBC iPlayer Indexing Tool and PVR

## Features

* Downloads MP4 streams from BBC iPlayer site with better quality than Flash player streams
* Downloads Flash AAC streams for radio programmes
* Allow multiple programmes to be downloaded using a single command
* Indexing of all available iPlayer programs
* Caching of Index (default 4h)
* Regex search on programme name 
* Regex search on programme description and episode title
* PVR capability (may be used from crontab)
* Full HTTP Proxy support
* Runs on Linux (Debian, Ubuntu, openSUSE and many others), OS X (10.5+) and Windows (XP/Vista/7/8)
* Requires perl 5.8.8+ with LWP and XML::Simple modules

**NOTE: get_iplayer can only search programmes broadcast within the previous 7 days, even if they are available for 30 days on the iPlayer web site.**  See [FAQ #1](https://github.com/get-iplayer/get_iplayer/wiki/faq).

## Parent Project

<https://github.com/get-iplayer/get_iplayer/>
	
## Installation (all platforms)

* pull repository 
`git pull <>`

* Build Docker Image
`docker build -t ${USER}/iplayer`

## Usage 

* Deploy iplayer docker image
`docker run -d -p 1935:1935 ${USER}/iplayer

* WebUI Access
At the moment there are a few issues with the deployment of the image. There are long delays before the UI comes online due to delays in the iplayer logic that needs to perform a refresh first. 

<http://127.0.0.1:1935>